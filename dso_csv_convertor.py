import csv
import struct
import sys
import argparse

import sigrok_session

# Import to dicts
# Scan rows for channels until they repeat
# Build dictionary of ch keys with rate and data values

analogueChannels = ['0','1','2','3']
digitalChannels = ['4','5','6','7','8','9','10','11']

def importToDict(path):
    chDict = {}
    
    with open(path, 'rb') as csvFile:
        csvReader = csv.DictReader(csvFile, restkey='data2')
        for row in csvReader:
            # Build data list for the row
            data = [row['data']] + row['data2']
            channel = row['channel']
            rate = int(float(row['rate'])) # Hopefully these are all the same!
            
            if channel not in chDict.keys():
                # Put it in
                chDict[channel] = {'rate':rate, 'data':data}
            else:
                # Append the data
                chDict[channel]['data'] += data
                
    return chDict
    
def convertData(dataDict):
    # Convert the data values in place in the dict
    # Analogue first
    for anaCh in analogueChannels:
        if anaCh in dataDict.keys():
            toFloat = lambda s : float(s)
            dataDict[anaCh]['data'] = list(map(toFloat, dataDict[anaCh]['data']))
            
    for digCh in digitalChannels:
        if digCh in dataDict.keys():
            toBool = lambda s : str(int(bool(int(s))))
            dataDict[digCh]['data'] = list(map(toBool, dataDict[digCh]['data']))
            
    return dataDict
            
def formatData(convertedDict):
    # Only do logic for now
    # Find the longest logic set
    chLengths = []
    for ch in digitalChannels:
        if ch in convertedDict.keys():
            chLengths.append(len(convertedDict[ch]['data']))
    maxLength = max(chLengths)
    
    # Convert channels' data into a list of bytes in base 2
    byteList = [''] * maxLength
    for ch in digitalChannels:
        data = convertedDict[ch]['data']
        for bitIndex in xrange(0, maxLength):
            if len(data) <= bitIndex:
                byteList[bitIndex] += '0' # Eh.
            else:
                byteList[bitIndex] += data[bitIndex]
                
    # Map to chars
    binToChar = lambda b : chr(int(b, 2))
    charList = bytearray(map(binToChar, byteList))
    
    return charList
    
def export(data, rate, path):
    session = sigrok_session.SigrokSession()
    session.set_rate(rate)
    session.set_data(data)
    fileObject = session.get_session_file().getvalue()
    
    if not path.startswith("./") and not path.startswith("/"):
        path = "./" + path
    if not path.endswith(".sr"):
        path += ".sr"
    with open(path, 'wb') as out:
        out.write(fileObject)

def main(argv):
    inputFile = ''
    outputFile = ''
    helpString = 'test.py -h for usage'
    
    parser = argparse.ArgumentParser()

    parser.add_argument("inPos", action="store", nargs="?", metavar="input")
    parser.add_argument("outPos", action="store", nargs="?", metavar="output")
    parser.add_argument('-i', action="store", dest="input")
    parser.add_argument("-o", action="store", dest="output")

    args = parser.parse_args(argv)

    inputFile = args.inPos if not args.input else args.input
    outputFile = args.outPos if not args.output else args.output
            
    if not inputFile:
        print(parser.parse_args(["-h"])) # Vomit out help to terminal
        sys.exit(2)
        
    if not outputFile:
        outputFile = inputFile[:inputFile.rfind(".")]
        
    rawDict = importToDict(inputFile)
    convertedDict = convertData(rawDict)
    rate = convertedDict[digitalChannels[0]]['rate']
    byteData = formatData(convertedDict)
    export(byteData, rate, outputFile)
    
if __name__ == '__main__':
    main(sys.argv[1:])
